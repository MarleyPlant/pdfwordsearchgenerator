#!/usr/bin/env python
from fpdf import FPDF
import string
import random
from re import sub
from progress.bar import Bar

debug = True


def camelCase(string):
    string = sub(r"(_|-)+", " ", string).title().replace(" ", "")
    return string[0].lower() + string[1:]


class WordSearchBoard:
    board = []
    wordlist = []
    boardW = None
    boardH = None
    pdf = None
    pageSize = 'a4'
    title = ''

    def __init__(self, title, size={'height': 20, 'width': 20, 'pageSize': 'a4'}, wordlist=[], font=[], pdf=None):
        self.boardW = size['width']
        self.boardH = size['height']
        self.pageSize = size['pageSize']
        if (font):
            self.font = font
        self.wordlist = wordlist
        self.title = title
        self.board = self.generateBoard(size['width'], size['height'])
        if not (pdf == None):
            self.pdf = pdf
        self.setupPDF()
        self.placeWordlist()
        self.populateEmpty()
        self.draw()

    def pickRandomX(self):
        return random.randint(0, self.boardW)

    def pickRandomY(self):
        return random.randint(0, self.boardH)

    def setWordlist(self, wordlist):
        self.wordlist = wordlist

    def setupPageSettings(self):
        self.epw = self.pdf.w - 2*self.pdf.l_margin
        self.th = self.pdf.font_size
        self.col_width = self.epw / self.width

        self.drawTitle()
        self.pdf.set_font(self.font[0], self.font[1], self.font[2])
        self.pdf.ln(20)

    def drawTitle(self):
        self.pdf.set_font(self.font[0], self.font[1], 18)
        self.pdf.cell(self.epw / 4)
        self.pdf.cell(self.epw / 2, 15, self.title.upper(),
                      align='C', border=True)

    def setupPDF(self):
        if not (self.pdf):
            self.pdf = FPDF('P', 'mm', self.pageSize)
        self.pdf.add_page()
        self.pdf.set_font(self.font[0], self.font[1], self.font[2])
        self.setupPageSettings()

    def generateBoard(self, w, h):
        self.width = w
        board = [['_' for x in range(w)] for y in range(h)]
        return board  # Return the generated 2d array

    def drawQuestions(self):
        self.pdf.ln(10)
        words = len(self.wordlist)
        self.pdf.set_font(self.font[0], self.font[1], self.font[2])
        cell_width = self.epw / 5
        columns = 5

        for c in range(0, 4):
            for r in range(columns):
                if((c*5) + r < words):
                    self.pdf.cell(cell_width, 10, self.wordlist[(
                        c*5) + r].upper(), align="C", border=0)
            self.pdf.ln(10)

    def pickRandomLetter(self):
        return random.choice(string.ascii_letters).upper()

    def drawONPDF(self):
        for row in self.board:
            for column in row:
                # Enter data in colums
                if(column):
                    self.pdf.cell(self.col_width, self.col_width,
                                  column, border=1, align='C')
            self.pdf.ln(self.col_width)

    def isWithinBounds(self, x, y):
        return self.isWithinBounds(x, y)

    def placeWordlist(self):
        self.bar = Bar('Processing ' + self.title, max=len(self.wordlist))

        for word in self.wordlist:
            self.placeWord(word)

        self.bar.finish()

    def isWordPlacementCorrect(self, x, y, word, direction):
        for i in range(0, len(word)):
            newx = (x + direction[0]*i)
            newy = (y + direction[1]*i)

            if (newx < 0 or newy < 0):
                return False

            if(len(self.board) < newy):
                return False
            if(len(self.board) < newx):
                return False

            if not (self.board[newy][newx]):
                return False

            if not (self.board[newy][newx] == "_"):
                return False

            continue
        return True

    def randomDirection(self):
        return random.choice(
            [[1, 0], [0, 1], [1, 1], [-1, -1], [1, -1], [-1, 1]])

    def getGridSize(self):
        return [self.col_width * self.boardPW, self.col_width * self.boardH]

    def placeWord(self, word):
        attemps = 0

        while True:
            if (attemps > 9999):
                if(debug):
                    print("Finding Location Failed: " + word)
                return False

            direction = self.randomDirection()

            xsize = self.boardW if direction[0] == 0 else self.boardW - len(
                word)
            ysize = self.boardH if direction[1] == 0 else self.boardH - len(
                word)
            attemps += 1

            x = random.randrange(0, xsize)
            y = random.randrange(0, ysize)

            if (self.isWordPlacementCorrect(x, y, word, direction)):
                break

        self.bar.next()
        for i in range(0, len(word)):
            self.board[y + direction[1]*i][x +
                                           direction[0]*i] = word[i].upper()

    def populateEmpty(self):
        yIndex = 0
        xIndex = 0

        for row in self.board:
            for column in row:
                if (not column or column == '_'):
                    self.board[yIndex][xIndex] = self.pickRandomLetter()
                xIndex += 1
            yIndex += 1
            xIndex = 0

    def generatePDF(self, fileName):
        if(fileName):
            self.pdf.output('output/' + camelCase(fileName) + '.pdf', 'F')

    def draw(self):
        self.drawONPDF()
        self.drawQuestions()

    def output(self, fileName):
        self.generatePDF(fileName)
