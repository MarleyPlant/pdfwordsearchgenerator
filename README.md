<h1><b>PyPDFWordsearchGenerator</b> - Generate PDF Wordsearch&#39;s With Python </h1>




## Getting Started

PYPDFWordsearch allows you to easily create single pdf pages or pdf books of multiple wordsearch pages. t
These can easily be generated programatically by providing a title, list of words and page size.

### Creating A Book

```python
from WordSearchBook import WordSearchBook
pageSettings = {'width': 15, 'height': 15, 'pageSize': 'A4'}
font = ['Arial', 'B', 12]

book = WordSearchBook('Example Wordsearch Book', pageSettings, None, font)
book.addPage('Page 1', [])
book.addPage('Page 2', [])
book.output()
```

The WordSearchBook class allows you to easily manage collections of WordSearchBoard's supply the font and page settings once then simply supply the title and worldlist for each page.

### Creating A Single Page

```python
from WordSearchBoard import WordSearchBoard
pageSettings = {'width': 15, 'height': 15, 'pageSize': 'A4'}
font = ['Arial', 'B', 12]
title = 'Example Wordsearch Board'
wordlist = []


board =  WordSearchBoard(title, pageSettings, wordlist, font)
board.output(title)
```

The WordSerchBoard class stores the board as well as functions for drawing and outputting the board as a pdf. it takes the same parameters as the book class however only generates a single page pdf.
## JSON
it is possible to write your wordsearches using JSON then simply generate a pdf using the python library.
this makes the books a lot easier to read and standardise. I will cover the basic syntax and how to export the JSON as a PDF
### Books

```json
{
  "title": "Bread Wordsearch",
  "pageSettings": { "width": 20, "height": 20, "pageSize": "A4" },
  "font": ["Arial", "B", 12],
  "pages": []
}
```

### Pages
```json
[
  {
    "title": "Example Wordsearch Board",
    "wordslist": ["word1", "word2"]
  }
]
```

### Exporting JSON to PDF
```python
from WordSearchBook import WordSearchBook

book = WordSearchBook()

# Import a whole book
book.importJSONBook('books/breadwordsearch.json')

# Or a set of pages
book.addJSONPages('pages/breadshapes.json')

book.output()
```

<br />

<br />


## 💻 **TECHNOLOGIES**
[![Python](https:&#x2F;&#x2F;img.shields.io&#x2F;badge&#x2F;Python-3776AB?style&#x3D;for-the-badge&amp;logo&#x3D;Python&amp;logoColor&#x3D;white)]()

<br />

