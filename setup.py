from setuptools import setup
setup(
     name='PYPDFWordsearch',    
     version='0.1',                  
     url='https://gitlab.com/MarleyPlant/pdfwordsearchgenerator',
     author='Marley Plant',
     author_email='marley@marleyplant.com',        
     scripts=['WordSearchBook'],
     install_requires=[
          'progress',
          'fpdf',
      ],
)